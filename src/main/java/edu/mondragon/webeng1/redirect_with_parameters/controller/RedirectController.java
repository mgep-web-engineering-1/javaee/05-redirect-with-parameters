package edu.mondragon.webeng1.redirect_with_parameters.controller;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "RedirectController", urlPatterns = { "/redirect" })
public class RedirectController extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private String message;

  public RedirectController() {
    super();
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    message = request.getParameter("message");
    if (message == null || message.equals("")) {
      response.sendRedirect(response.encodeRedirectURL("error.jsp?error=" + "Message not found"));
    } else {
      response.sendRedirect(response.encodeRedirectURL("ok.jsp?message=" + message));
    }
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }

}
