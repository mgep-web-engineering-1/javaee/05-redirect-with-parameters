<%@ page language="java" contentType="text/html; charset=UTF8"
    pageEncoding="UTF8"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>WRONG</title>
</head>

<body>
  <h1>Something WRONG (<%= request.getParameter("error") %>)</h1>
</body>

</html>