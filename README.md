# Redirect Parameters in RedirectController

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run ```src > main > java > ... > Main.java```.
* Open [http://localhost:8080](http://localhost:8080) in the browser.

## Objective

We are going to send the parameter to the second request whith ```RedirectController```.

## Explaination

* ```index.html``` form will send the parameter to the controller.
* The controller will add the parameter to the redirect URL.
* So the browser will make the 2nd request with the parameters in the URL using GET.
* Even if the form makes a POST request to the controller, the second request to ```ok.jsp``` wont be using POST.

```java
// RedirectController.java
...
response.sendRedirect(response.encodeRedirectURL("ok.jsp?message="+message));
...
```

* ```encodeRedirectURL``` is used so special characters are correctly sent (spaces, *ñ*, ...).

## Next and before

* Before [04-servlet-redirect](https://gitlab.com/mgep-web-engineering-1/javaee/04-servlet-redirect)
* Next [06-server-redirect-with-session](https://gitlab.com/mgep-web-engineering-1/javaee/06-server-redirect-with-session)